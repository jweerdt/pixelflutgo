package main

import (
    "fmt"
    "io/ioutil"
    "os"
    "log"
	"strconv"
	"net"
//	"time"
)

// Declare vars

const foreground = "ffffff"
const background = "000000"

var filepointer int = 0
var needtosend byte = 0

const xoffset int = 280
const yoffset int = 380

var xscreen	int = 0
var yscreen int = 0
var colour string = "000000"

func main() {

// Open txt file

    file, err := os.Open("input.txt")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()


  txtfile, err := ioutil.ReadAll(file)
  fmt.Print("Opened file" + "\n")
//fmt.Print(txtfile)

	conn, _ := net.Dial("tcp", "pixelflut.event.campzone.nl:1234")
// conn, _ := net.Dial("tcp", "204.2.68.116:1234") // Connect to server
	print("Sending data to pixelflut @ CampZone 2019") // Print start message

// Start loop pretending to be a while 1
for i := 0; i < 105; i = 1 {

	xscreen++
	filepointer++

	if txtfile[filepointer] == 121 {
	yscreen = yscreen + 1
	xscreen = 0
	}
	if txtfile[filepointer] == 120 {
		xscreen = 0
		yscreen = 0
		filepointer = 0
	}
	if txtfile[filepointer] == 48 {
		colour = background
		// print("Background pixel" + "\n")
		needtosend = 1
	}
	if txtfile[filepointer] == 49 {
		colour = foreground
		// print("Foreground pixel" + "\n")
		needtosend = 1
	}

	// xscreen = xscreen + 1
	// filepointer = filepointer + 1

	if needtosend == 1 {
	strxscreen := strconv.Itoa(xscreen + xoffset) // parse Int into string
	stryscreen := strconv.Itoa(yscreen + yoffset)	// parse Int into string
	var msg string = "PX " + strxscreen + " " + stryscreen + " " + colour // compile package
	conn.Write([]byte(msg + "\n")) // Send package
	// print("I just send:" + msg + "\n")
	}
	// time.Sleep(100)
  }
}
